<script type="text/javascript">
jQuery(document).ready(function($){
	//$('#color_picker_header_bg_color').farbtastic('#header-bg-color');
	
	//var colorPicker = $.farbtastic("#color_picker_header_bg_color");
	$('#color_picker_header_bg_color').farbtastic(function() {
		$("#header-bg-color").val($.farbtastic('#color_picker_header_bg_color').color);
		$(".mobile-background-preview").css("background-color",$("#header-bg-color").val());
	});
	
	$('#header-bg-color').change(function() {
		$(".mobile-background-preview").css("background-color",$("#header-bg-color").val());
	});
});
</script>
<div class="wrap">
	<?php screen_icon( 'plugins' ); ?>
	<h2>Mobile Page Settings</h2>
	<hr/>
    <form method="POST" action="">
    
	<?php 
	$wp_version = get_bloginfo( 'version' ); 
	if($wp_version < 3.4){
	?>
	<div style='display: inline-block; padding: 20px; margin-bottom: 20px; background-color: #E5B4B1; border: solid 1px #A4362D; color: #E13C33'><span style='font-size: 18px; display:block; margin-bottom:0px'><p>Wordpress version is <strong style="color: #ffc;"><?php echo $wp_version?></strong></p><p>You need version <strong style="color: #ffc;">3.4</strong> or newer to run this mobile plugin</p></span></div>
	<?php } ?>
    
	<h2>Logos</h2>
	<p><a href="<?php echo WP_PLUGIN_URL."/wp-mobile-pages-v3/theme/".MOBILE_THEME."/images/mobile-logo-2x.psd" ?>">Download PSD logo guide</a></p>
	<h4>Apple Touch Home Screen Icon</h4>
	<p>This icon is displayed when you bookmark the site to the home screen of an IOS device. Make sure this is square. <strong>150x150</strong> should be big enough for the range of different devices, and will shrink down for lower resolution devices. IOS automatically adds the following effects: rounded corners, drop shadow, and reflected shine. No need to do any of that yourself.</p>
	<input size="80" name="options[touch-icon]" value="<?php echo $options['touch-icon'] ?>" />
	<img style="clear: both; display: block;" src="<?php echo $options['touch-icon'] ?>" />
	<hr style="border: none; border-bottom: 2px dotted #ddd;"/>
	<h4>Standard Logo</h4>
	<ul>
		<li><em>Max width of 320px</em></li>
		<li><strong>Image Location: </strong></li>
		<li><input size="80" name="options[logo-url]" value="<?php echo $options['logo-url'] ?>" /></li>
		<li><div class="mobile-background-preview" style="width: 320px; height: <?php echo $options['header-height'] ?>px; overflow: hidden; display: inline-block; background-color: <? echo $options['header-bg-color'] ?>" ><img src="<?php echo $options['logo-url']; ?>"/></div></li>
	</ul>
	<hr style="border: none; border-bottom: 2px dotted #ddd;"/>
	<h4>Retina Logo</h4>
	<ul>
        <li><em>This logo must exactly twice the size, and be in the same location as the first with -2x at the end. </em></li>
        <li><strong>Image Location: </strong></li>
		<li><input disabled="disabled" size="80" name="options[retina-logo-url]" value="<?php echo $options['retina-logo-url'] ?>" /></li>
        <li class="mobile-background-preview" style="width: 640px; height: <?php echo ($options['header-height'] * 2) ?>px; overflow: hidden; display: inline-block; background-color: <? echo $options['header-bg-color'] ?>"><img src="<?php echo $options['retina-logo-url']; ?>"/></li>
	</ul>
	<hr style="border: none; border-bottom: 2px dotted #666;"/>
	<h2>Header</h2>
	<p>
		<strong>Header pixel height</strong> <input maxlength="3" size="3" name="options[header-height]" value="<?php echo $options['header-height'] ?>"  /> <em>(default is 110)</em>
	</p>
	<h4>Header background color</h4>
	<p>
		<input type="text" id="header-bg-color" value="<?php echo $options['header-bg-color'] ?>" name="options[header-bg-color]" />
		<div id="color_picker_header_bg_color"></div>
	</p>
	
	<hr style="border: none; border-bottom: 2px dotted #666;"/>
	<h2>Custom Styling</h2>
	<p><strong>Markup Structure: </strong>#header-container > #header > #logobox > #logo > img</p>
	<p>
		<textarea name="options[custom-css]" style="width: 600px; height: 250px; border: 3px solid #cccccc; padding: 5px;"><?php echo $options['custom-css'] ?></textarea>
	</p>
	<? /*
	<ul>
		<li>#header-container</li>
		<li>
			<ul>
				<li>#header</li>
				<li>
					<ul>
						<li>#logobox</li>
						<li>
							<ul>
								<li>#logo</li>
								<li>
									<ul>
										<li>img</li>
									</ul>
								</li>
							</ul>
						</li>
					</ul>
				</li>
			</ul>
		</li>
	</ul>
	*/ ?>
	<br/>
	<hr style="border: none; border-bottom: 2px dotted #666;"/>
	<h2>Wufoo Form ID</h2>
	<?php if ($options['wufoo-form-id'] == 's7s0z9'){; ?><p><span style="padding: 6px; color:#ffc; font-size: 14px; background:#f50;">This is the default Roadside wufoo account. You need to set this to the client's wufoo form ID.</span></p><?php } ?>
	<input size="10" name="options[wufoo-form-id]" value="<?php echo $options['wufoo-form-id'] ?>" />
	<br/>
	<hr style="border: none; border-bottom: 2px dotted #666;"/>
	<?php if( function_exists('cinfotag') ) : ?>

	<h2>Contact Info</h2>
	<p>
		<table class="widefat contact-info"> 
			<thead>
				<tr>
					<th style="width:80px">Enabled</th>
					<th style="width:140px">Item</th>
					<th style="width:150px">URL</th>
					<th >Description<th>
				</tr>
			</thead>
			
			<tbody>
				<tr class="alternate">
					<td><input type="hidden" checked="checked" name="enabled_items[]" value="full-name" /></td>
					<td>Full Name</td>
					<td><input maxlength="255" size="35" name="options[fullname]" value="<?php echo cinfotag('full-name') ?>" disabled="disabled" /></td>
					<td>Using {{full-name}} <a href="options-general.php?page=client-info&amp;ci_page=edit">Client Info</a> Tag</td>
				</tr>
				<tr>
					<td><input type="hidden" checked="checked" name="enabled_items[]" value="email"/></td>
					<td>Email</td>
					<td><input maxlength="255" size="35" name="options[email]" value="<?php echo cinfotag('email') ?>" disabled="disabled" /></td>
					<td>Using {{email}} <a href="options-general.php?page=client-info&amp;ci_page=edit">Client Info</a> Tag</td>
				</tr>
				<tr class="alternate">
					<td><input type="hidden" checked="checked" name="enabled_items[]" value="address1"/></td>
					<td>Address Line 1</td>
					<td><input maxlength="255" size="35" name="options[address1]" value="<?php echo cinfotag('address1') ?>" disabled="disabled" /></td>
					<td>Using {{address1}} <a href="options-general.php?page=client-info&amp;ci_page=edit">Client Info</a> Tag</td>
				</tr>
				<tr>
					<td><input type="hidden" checked="checked" name="enabled_items[]" value="address2"/></td>
					<td>Address Line 2</td>
					<td><input maxlength="255" size="35" name="options[address2]" value="<?php echo cinfotag('address2') ?>" disabled="disabled" /></td>
					<td>Using {{address2}} <a href="options-general.php?page=client-info&amp;ci_page=edit">Client Info</a> Tag</td>
				</tr>
				<tr class="alternate">
					<td><input type="hidden" checked="checked" name="enabled_items[]" value="phone"/></td>
					<td>Phone</td>
					<td><input maxlength="255" size="35" name="options[phone]" value="<?php echo cinfotag('phone') ?>" disabled="disabled" /></td>
					<td>Using {{phone}} <a href="options-general.php?page=client-info&amp;ci_page=edit">Client Info</a> Tag</td>
				</tr>
				<tr>
					<td><input type="hidden" checked="checked" name="enabled_items[]" value="fax"/></td>
					<td>Fax</td>
					<td><input maxlength="255" size="35" name="options[fax]" value="<?php echo cinfotag('fax') ?>" disabled="disabled" /></td>
					<td>Using {{fax}} <a href="options-general.php?page=client-info&amp;ci_page=edit">Client Info</a> Tag</td>
				</tr>
				<tr class="alternate">
					<td><input type="checkbox"<?php echo mobile_pages_item_enabled("map-url") ? 'checked="checked"' : ''; ?> name="enabled_items[]" value="map-url"/></td>
					<td>Map URL</td>
					<td><input maxlength="255" size="35" name="options[map-url]" value="<?php echo cinfotag('map-url') ?>" disabled="disabled" /></td>
					<td>Using {{map-url}} <a href="options-general.php?page=client-info&amp;ci_page=edit">Client Info</a> Tag</td>
				</tr>
			</tbody>
		</table>
	</p>
	</br>
	
	<h2>Social Info</h2>	
	<p>
		<table class="widefat social-info"> 
			<thead>
				<tr>
					<th style="width:80px">Enabled</th>
					<th style="width:140px">Item</th>
					<th style="width:150px">Value</th>
					<th>Description<th>
				</tr>
			</thead> 
			<tbody>
				<tr class="alternate">
					<td><input type="checkbox"<?php echo mobile_pages_item_enabled("youtube-url") ? 'checked="checked"' : ''; ?> name="enabled_items[]" value="youtube-url"/></td>
					<td>Youtube</td>
					<td><input maxlength="255" size="35" name="options[youtube-url]" value="<?php echo cinfotag('youtube-url') ?>" disabled="disabled" /></td>
					<td>Using {{youtube-url}} <a href="options-general.php?page=client-info&amp;ci_page=edit">Client Info</a> Tag</td>
				</tr>
				<tr class="">
					<td><input type="checkbox"<?php echo mobile_pages_item_enabled("facebook-url") ? 'checked="checked"' : ''; ?> name="enabled_items[]" value="facebook-url"/></td>
					<td>Facebook</td>
					<td><input maxlength="255" size="35" name="options[facebook-url]" value="<?php echo cinfotag('facebook-url') ?>" disabled="disabled" /></td>
					<td>Using {{facebook-url}} <a href="options-general.php?page=client-info&amp;sci_page=edit">Client Info</a> Tag</td>
				</tr>
				<tr class="alternate">
					<td><input type="checkbox"<?php echo mobile_pages_item_enabled("twitter-url") ? 'checked="checked"' : ''; ?> name="enabled_items[]" value="twitter-url"/></td>
					<td>Twitter</td>
					<td><input maxlength="255" size="35" name="options[twitter-url]" value="<?php echo cinfotag('twitter-url') ?>" disabled="disabled" /></td>
					<td>Using {{twitter-url}} <a href="options-general.php?page=client-info&amp;ci_page=edit">Client Info</a> Tag</td>
				</tr>
				<tr class="">
					<td><input type="checkbox"<?php echo mobile_pages_item_enabled("linkedin-url") ? 'checked="checked"' : ''; ?> name="enabled_items[]" value="linkedin-url"/></td>
					<td>LinkedIn</td>
					<td><input maxlength="255" size="35" name="options[linkedin-url]" value="<?php echo cinfotag('linkedin-url') ?>" disabled="disabled" /></td>
					<td>Using {{linkedin-url}} <a href="options-general.php?page=client-info&amp;ci_page=edit">Client Info</a> Tag</td>
				</tr>
				<tr class="alternate">
					<td><input type="checkbox"<?php echo mobile_pages_item_enabled("flickr-url") ? 'checked="checked"' : ''; ?> name="enabled_items[]" value="flickr-url"/></td>
					<td>Flickr</td>
					<td><input maxlength="255" size="35" name="options[flickr-url]" value="<?php echo cinfotag('flickr-url') ?>" disabled="disabled" /></td>
					<td>Using {{flickr-url}} <a href="options-general.php?page=client-info&amp;ci_page=edit">Client Info</a> Tag</td>
				</tr>
				<tr class="">
					<td><input type="checkbox"<?php echo mobile_pages_item_enabled("google-plus-url") ? 'checked="checked"' : ''; ?> name="enabled_items[]" value="google-plus-url"/></td>
					<td>Google+</td>
					<td><input maxlength="255" size="35" name="options[google-plus-url]" value="<?php echo cinfotag('google-plus-url') ?>" disabled="disabled" /></td>
					<td>Using {{google-plus-url}} <a href="options-general.php?page=client-info&amp;ci_page=edit">Client Info</a> Tag</td>
				</tr>
				<tr class="alternate">
					<td><input type="checkbox"<?php echo mobile_pages_item_enabled("review-google-url") ? 'checked="checked"' : ''; ?> name="enabled_items[]" value="review-google-url"/></td>
					<td>Google Places / Google Review</td>
					<td><input maxlength="255" size="35" name="options[review-google-url]" value="<?php echo cinfotag('review-google-url') ?>" disabled="disabled" /></td>
					<td>Using {{review-google-url}} <a href="options-general.php?page=client-info&amp;ci_page=edit">Client Info</a> Tag</td>
				</tr>
			</tbody>
		</table>
	</p>	
	<?php else : ?>
		
	<div style='display: inline-block; padding: 20px; margin-bottom: 20px; background-color: #E5B4B1; border: solid 1px #A4362D; color: #E13C33'><span style='font-size: 24px; display:block; margin-bottom:0px'>Please Install the <a href="http://plugins.nucleartuxedo.com/extend/plugins/client-info-tags/">Client Info Tags Plugin</a></span><br><span style="color: #000;">Contact information and links are controlled by the client info tags plugin. Please visit this page when you've installed the client info tags plugin. Did I mention client info tags plugin? If you're still reading this, you still need the client info tags plugin.</span></div>
		
	<?php endif; ?>
	<br>
	<input type="hidden" name="mobile_pages_admin_submit" />
	<p><input type="submit" name="Save" value="Save Options" class="button-primary" /></p>
	
	
    </form>
</div>