=== WP Mobile Pages v3 ===
Contributors: Milo Jennings, Colby Sollars, Ryan P.C. McQuen
Tags: 
Requires at least: 3.4
Tested up to: 4.2.2
Version: 3.11
Stable tag: 3.11

Dynamically loads an included mobile theme to display mobile pages and blog posts on mobile devices.

== Description ==

* Displays mobile theme when viewing mobile pages and blog posts on mobile devices
* Includes a custom mobile theme designed for modern mobile devices
* Allows customization of theme logo via an easy to use settings page
* Registers a new Custom Post Type for mobile specific pages
 
= Shortcodes =

**[mobile_contact_info]**

Outputs mobile friendly contact info. Uses the client info tags plugin as it's data source. The arguments provided tell it which client info tag keys to use for the given argument.

Default arguments for [mobile_contact_info]

* **email:** email (string: client info tag key name)
* **phone:** phone (string: client info tag key name)
* **map_url:** true (boolean: true or false)
* **address1:** address1 (string: client info tag key name)
* **address2:** address2 (string: client info tag key name)

**[mobile_gallery]**

Outputs a mobile gallery using the images attached to the given page.

Default arguments for [mobile_gallery]

* captions: false (boolean: true or false) 

**[youtube_gallery]**

Ouputs a wrapper for [youtube_thumb] shortcodes

**[youtube_thumb]**

Default arguments for [youtube_thumb]

* **id:** false (string: the id of a youtube video)
* **title:** false (string: Caption below video thumbnail)


== Changelog ==

Version 3.11 [03/29/2017]

- Removed outdated Quark plugin updater code that was causing sitemaps to have a whitespace issue.

Version 3.10 [08/04/2015]

 - Added company name to top of contact info in footer. If {{company}} tag not available, it falls back to {{full-name}}
 - Added bitbucket info for Github updater plugin

Version 3.09 [04/08/2015]

 - Updated mobile detect to 2.8.12

Version 3.08 [08/05/2014]

 - New icon for ReviewPro (also replaces Google Places icon).

Version 3.07 [03/20/2014]

 - Fixed hybrid prefix. Was causing error with PHP 5.4

Version 3.05 [08/01/2013]

 - Fixed Android DPI size

Version 3.04 [01/26/2013]

 - Windows 8 phone fixes
    - Upgraded mobile detection. Should be better at detecting newer Windows phones
    - Fixed borders around linked images
    - Fixed ugly rendering of gradients and other graphics
    - Added new meta tags to prevent grey highlighting on touch events
    - Fixed retina issues
 - Updated the menu button to a more semantic standard
 - Made single video use high quality thumbnail for single videos, such as AMP
 - Minified CSS files
 - Switched numerous JS files to use CDN's with Gzipping
 - Made photoswipe only load in on pages that contain a gallery to fix javascript error and optimize bandwidth usage
 - Reduced overall download from 267KB to 120KB

Version 3.03 [12/31/2012]

 - Fixed issue that prevented some youtube video thumbnails from showing up
 - Fixed issue that misidentified broken youtube links

Version 3.02 [12/31/2012]

 - Fixed embedded youtube video issue on IOS4 devices and Android devices
 - Minor CSS updates
 - Map button now opens directly to Apple maps app instead of in safari google maps version on IOS6. Confirmed working with Android.
 - Fixed permalink flushing issue that caused blank screen when upgrading to mobile 3.0 from 0.1.4
 - Now prevents many unneeded plugins from loading JS and CSS files.
 - Hides WP socializer on mobile pages
 - Updated mobile detection script from 2.4 to 2.5.2 (hopefully this helps detect more Windows 8 devices)
 - Made phone number and email address in footer clickable
 - Fixed phone number issue that appears when using older versions of call tracking replacement plugin
 - Added Isotope JS for responsive thumbnail layouts
 - Added favicon reference to root of site
 - Added script query var removal line for better caching support
 - Fixed issue with errors returned when youtube videos are not available or invalid. Now displays an error only if the user is logged in. Otherwise it silently fails.

Version 3.01 [12/04/2012]

 - Map button automatically added to top of directions page on upgrade
 - Made videos go to embedded version of youtube video, so there's no other videos listed
 - Finish video page (set height to be proportionate)
 - Fixed search results
 - Fixed Farbtastic color picker
 - Add video overlay icon - make it retina compatible
 - Setup default page creation with video gallery shortcode example built in
 - set videos container to minumum size, if thumbnails don't load quickly
 - Added phone number next to icon
 - Added Video Gallery Section
 - Fixed mobile page settings - duplicate links to download PSD
 - Gallery captions are now disabled by default. They can be enabled using the shortcode like so [mobile_gallery captions="true"]

Version 3.0 [12/03/2012]

 - Rebuilt from the ground up, big improvements over version 0.1.4 of mobile plugin
 - Mobile theme included with plugin
 - Uses client info tags
 - Hides social tabs
 - Automatically adds contact info shortcode to contact us page when upgrading from old mobile
 - Setup Contact Us page to support second address
 - Deletes old theme from themes folder when activated
 - Optimized detection technology
 - Prevents thumbnails from showing up on archives pages
 - Added warnings for pre 3.4 versions of wordpress
 - Added custom CSS admin interface
 - Made wufoo optional
 - Setup wufoo form to not appear at all if Form ID is empty
 - Fixed Photoswipe gallery zooming issue
 - Flipped gallery caption and toolbar
 - Added filter to prevent AddThis links from appearing on mobile pages
 - Figured out how to keep theme in plugin folder
 - Hid gallery caption
 - Added link to PSD in admin page
 - Home Screen Icon
 - Stopped asset copying from happening
 - Made default logo location the plugin folder
 - Created setting for Wufoo Form ID
 - Added client info tags override
 - Move Social buttons to bottom above full site link
 - Added phone & fax to bottom
 - Updated retina jQuery plugin
 - Added Wufoo slideout form
 - Mobile Gallery jQuery Plugin
 - Restructured WP options configuration
 - Made plugin updates work
 - Warning shows when wufoo form ID is set to default Roadside account
 - Added wufoo form ID to admin page
 - Made wufoo form ID default var on activation & update
 - Made into parent theme
 - Grabs and deletes old mobile options upon activation
 
