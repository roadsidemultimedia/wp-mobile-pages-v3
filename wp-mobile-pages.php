<?php
/*
Plugin Name: WP Mobile Pages v3
Plugin URI: https://bitbucket.org/roadsidemultimedia/wp-mobile-pages-v3
Description: Dynamically loads an included mobile theme to display mobile pages and blog posts on mobile devices.
Branch: master
Bitbucket Plugin URI: https://bitbucket.org/roadsidemultimedia/wp-mobile-pages-v3
Bitbucket Branch: master
Author: Colby Sollars & Milo Jennings & Ryan P.C. McQuen
Version: 3.11
*/

// Set plugin version, never use more than 1 decimal, 
// to keep mathematical comparison operators functional
define('MOBILE_PAGES_VERSION', 3.11);

/**
 * Switch to Mobile version of site when visiting "/mobile"
 * Also redirects "/mobile" to "/mobile/index" as opening page
 */

define('MOBILE_THEME', 'rsmm-mobile-v3');
define('MOBILE_SUBDIRECTORY', 'mobile');
define('MOBILE_INDEX', site_url().'/'.MOBILE_SUBDIRECTORY.'/index');
define('MOBILE_PAGES_PATH', plugin_dir_path(__FILE__) );

register_activation_hook(__FILE__, 'mobile_pages_activate');
register_deactivation_hook(__FILE__, 'mobile_pages_deactivate');

function mobile_pages_activate() {
	flush_rewrite_rules(false);
}

function mobile_pages_deactivate() {
	$options = get_option('mobile_page_options');
	$options['links-flushed'] = 'false';
	flush_rewrite_rules(false);
	update_option( 'mobile_page_options', $options );
}


/* Set Wordpress to search plugin folder for mobile theme */

add_action('init', 'mobile_pages_init');
function mobile_pages_init() {

	//Check if version variable exists, and if it doesn't match, run the upgrade
	$version_option = get_option( 'mobile_pages_version');
	if ( false === $version_option || ! isset( $version_option ) || $version_option < MOBILE_PAGES_VERSION ) {
			// If no mobile pages plugin has ever been installed, set version to zero,
			// set all default options & import old entries if they exist
			$current_version = isset( $version_option ) ? $version_option : 0;
			mobile_pages_upgrade( $current_version );
			update_option( 'mobile_pages_version', MOBILE_PAGES_VERSION );
	}
}


register_theme_directory( 'plugins/wp-mobile-pages-v3/theme');

$mobile_optional_patterns = array(
	'\/blog\/.*',
	'\/archives\/.*',
	'\/\d{4}\/\d{2}\/.*',
	'\/\?s=.*'
);
include 'register-custom-post-type.php';

function mobile_pages_upgrade( $version ){
	
	// if ( $version == 4 ){
	// 		add_option( 'mobile_page_version_test', "it worked, you're upgrading to version 4" );
	// 	}
	//if upgrading from pre 3.0 run full upgrade process
	if ( $version < 1 ){
	
		/* Add contact info shortcode to contact us page */
		$contact_page_update = array();
		$contact_page_object = get_page_by_path('contact-us', 'OBJECT', 'mobile_page');
		if ($contact_page_object) {
			//echo "<h1>contact object exists</h1>";
			$contact_page_content = $contact_page_object->post_content;
			$contact_page_update['ID'] = $contact_page_object->ID;
			
			//check if contact info shortcode exists, and if not, add it
			if ( false !== strpos($contact_page_content, '[mobile_contact_info' ) ) {
			} else {
				$contact_page_update['post_content'] = $contact_page_content . "\n" . '[mobile_contact_info address1="address1" address2="address2" email="email" phone="phone" map_url="map-url"]';
				// Update the post into the database
				wp_update_post( $contact_page_update );
			}
		}
		/* Add map button shortcode to directions page */
		$directions_page_update = array();
		$directions_page_object = get_page_by_path('directions', 'OBJECT', 'mobile_page');
		if ($directions_page_object) {
			//echo "<h1>contact object exists</h1>";
			$directions_page_content = $directions_page_object->post_content;
			$directions_page_update['ID'] = $directions_page_object->ID;
			
			//check if contact info shortcode exists, and if not, add it
			if ( false !== strpos($directions_page_content, '[mobile_contact_info' ) ) {
			} else {
				$directions_page_update['post_content'] = '[mobile_contact_info address1="address1" address2="address2" email="false" phone="false" map_url="map-url"]' . "\n" . $directions_page_content;
				// Update the post into the database
				wp_update_post( $directions_page_update );
			}
		}
	
	
		$custom_css_default = '#logobox {'."\n";
		$custom_css_default .= '	width: 100%;'."\n";
		$custom_css_default .= '}'."\n";
		$custom_css_default .= '#logo {'."\n";
		$custom_css_default .= '	margin: 0 auto;'."\n";
		$custom_css_default .= '	position: relative;'."\n";
		$custom_css_default .= '	width: 320px;'."\n";
		$custom_css_default .= '	text-align: center;'."\n";
		$custom_css_default .= '}'."\n";
	
		$new_options = array(
			'enabled_items' => array(
			    "full-name",
				"email",
				"phone",
				"fax",
				"address1",
				"address2",
				"map-url",
				"youtube-url",
				"facebook-url",
				"twitter-url",
				"linkedin-url",
				"flickr-url",
				"google-plus-url",
				"review-google-url"
			),
			'header-bg-color' => 'transparent',
			'header-height' => '110',
			'touch-icon' => WP_PLUGIN_URL."/wp-mobile-pages-v3/theme/".MOBILE_THEME."/images/touch-icon.png",
			'logo-url' => WP_PLUGIN_URL."/wp-mobile-pages-v3/theme/".MOBILE_THEME."/images/mobile-logo.png",
			'retina-logo-url' => WP_PLUGIN_URL."/wp-mobile-pages-v3/theme/".MOBILE_THEME."/images/mobile-logo-2x.png",
			'links-flushed' => 'false',
			'custom-css' => $custom_css_default,
			'wufoo-form-id' => 's7s0z9'
		);
		/* if old options from wp mobile pages 0.1.3 exist, update to new system and delete old structure */
	
		delete_option( 'mobile_pages_youtube_url' );
		delete_option( 'mobile_pages_facebook_url' );
		delete_option( 'mobile_pages_twitter_url' );
		delete_option( 'mobile_pages_youtube_url' );
		delete_option( 'mobile_pages_map_url' );
		delete_option( 'mobile_pages_fullname_var' );
		delete_option( 'mobile_pages_email_var' );
		delete_option( 'mobile_pages_phone_var' );
		delete_option( 'mobile_pages_address1_var' );
		delete_option( 'mobile_pages_address2_var' );
		delete_option( 'mobile_pages_enabled_items' );
	
		if( $existing = get_option( 'mobile_pages_header_bg_color_var' ) )
			$new_options['header-bg-color'] = $existing;
		delete_option( 'mobile_pages_header_bg_color_var' );
	
		if( $existing = get_option( 'mobile_pages_header_height_var' ) )
			$new_options['header-height'] = $existing;
		delete_option( 'mobile_pages_header_height_var' );
	
		if( $existing = get_option( 'mobile_pages_logo_url' ) )
			$new_options['logo-url'] = $existing;
		delete_option( 'mobile_pages_logo_url' );
	
		if( $existing = get_option( 'mobile_pages_retina_logo_url' ) )
			$new_options['retina-logo-url'] = $existing;
		delete_option( 'mobile_pages_retina_logo_url' );
	
		delete_option( 'mobile_pages_calltrackingnumber_var' );
		delete_option( 'mobile_pages_linkedin_url' );
		delete_option( 'mobile_pages_links_flushed');
		
		//save options
		add_option( 'mobile_page_options', $new_options );
		
		//Delete old theme
		delete_files(get_theme_root()."/rsmm-mobile-theme");
		
		$options = get_option('mobile_page_options');
		if( $options['links-flushed'] == 'false' ) {
			flush_rewrite_rules(false);
			$options['links-flushed'] = 'true';
			update_option( 'mobile_page_options', $options );
		}
	}
}

function delete_files($path){
	if (is_dir($path) === true){
		$files = array_diff(scandir($path), array('.', '..'));
		foreach ($files as $file){
			delete_files(realpath($path) . '/' . $file);
		}
		return rmdir($path);
	}
	else if (is_file($path) === true){
		return unlink($path);
	}
	return false;
}

// Register admin options page
add_action('admin_menu' , 'register_mobile_settings_page');
if (isset($_GET['page']) && $_GET['page'] == 'wp-mobile-pages.php') {
    add_action('admin_print_scripts', 'mobile_pages_admin_scripts');
    add_action('admin_print_styles', 'mobile_pages_admin_styles');
}
function mobile_pages_admin_scripts() {
    wp_enqueue_script('thickbox');
    wp_enqueue_script('farbtastic');
}

function mobile_pages_admin_styles() {
	wp_enqueue_style('thickbox');
	wp_enqueue_style('farbtastic');
}
 
function register_mobile_settings_page() {
    add_submenu_page('edit.php?post_type=mobile_page', 'Mobile Page Settings', 'Mobile Page Settings', 'update_themes', basename(__FILE__), 'mobile_settings_page');
}

function mobile_settings_page(){
	
	if(isset($_POST['mobile_pages_admin_submit'])) {
		
		$old_options = get_option('mobile_page_options');
		
		// get data from form
		$new_options = $_POST['options'];
		
		// acquire path of retina version
		$logo_withoutExt = preg_replace("/\\.[^.\\s]{3,4}$/", "", $new_options['logo-url']);
		$logo_file_extension = substr($new_options['logo-url'], strrpos($new_options['logo-url'], '.') + 1);
		$new_options['retina-logo-url'] = $logo_withoutExt . "-2x." . $logo_file_extension;
		
		// add checked items before saving option
		$new_options['enabled_items'] = serialize($_POST['enabled_items']);
		
		// merge existing options
		$array_merge = array_merge( $old_options, $new_options );
		
		// Write settings to options
		update_option('mobile_page_options', $new_options);
		
	} 
	
	// Read options for input field values
	$options = get_option('mobile_page_options');
	
	include('admin-page.php');
	
}

$display_theme = '';



if(!is_admin()){
	if(function_exists('wp_get_theme')){
    	add_filter('template', 'mobile_pages_template');
	    add_filter('stylesheet', 'mobile_pages_stylesheet');
	}
}

function mobile_pages_item_enabled($item_name) {
	$options = get_option('mobile_page_options');
	$enabled_items = $options['enabled_items'];

	if( is_string($enabled_items) )
		$enabled_items = unserialize($enabled_items);
		
	if(!$enabled_items)
		$enabled_items = array();
		
	if(in_array($item_name, $enabled_items))
		return True;
		
	return False;
}

function mobile_pages_template($template) {
    global $display_theme;
    $theme = mobile_pages_get_theme();
    $display_theme = $theme->Stylesheet;
    return $theme->Template;
}

function mobile_pages_stylesheet($stylesheet) {
    global $display_theme;
    $theme = mobile_pages_get_theme();
    $display_theme = $theme->Stylesheet;
    return $theme->Stylesheet;
}

function mobile_pages_get_theme() {
    global $mobile_optional_patterns;
    global $display_theme;
    // If we've already been through all this crap, no need to repeat it.
    if($display_theme != '') {
	    if(function_exists('wp_get_theme'))
    	    return wp_get_theme($display_theme);
    }
    
    // Optionally force a theme on optional pages
    if(isset($_GET['mobile'])) {
        $get_mobile = strtolower($_GET['mobile']);
        if($get_mobile == 'true')
            mobile_pages_set_cookie('mobile');
        if($get_mobile == 'false')
            mobile_pages_set_cookie('desktop');
    }
   
    $desktop_theme = get_option('stylesheet');
    $mobile_theme = MOBILE_THEME;
	$real_request_full = str_replace(str_replace('https://', '', str_replace('http://', '', site_url() )), '', $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
    $real_request = str_replace("?".$_SERVER['QUERY_STRING'], '', $real_request_full);
	$request_parts = explode('/', ltrim($real_request, '/'));
	$mobile_indicator = $request_parts[0];

	
	// If it's the mobile folder, redirect to the mobile index
    if($real_request == '/'.MOBILE_SUBDIRECTORY || $real_request == '/'.MOBILE_SUBDIRECTORY.'/') {
        if(!isset($_GET['s'])) {
		    mobile_pages_set_cookie('mobile');
		    wp_redirect(MOBILE_INDEX);
		    exit();
        }
    }
	
	// If subdirectory indicates its mobile, it is
	if($mobile_indicator == MOBILE_SUBDIRECTORY) {
		mobile_pages_set_cookie('mobile');
		return wp_get_theme($mobile_theme);
	}
	
	include_once('Mobile_Detect.php');
	$mobile_detect = new Mobile_Detect();
	if ($mobile_detect->isMobile()){
		$is_mobile = true;
		if ($mobile_detect->isTablet()){
			$is_mobile = false;
		}
	}
	
	
	/*
		If the request indicates that it can optionally be displayed with the
		mobile theme, then we'll set the cookie and return the mobile theme.
	*/
    
    // If it's the root of the site, redirect to the mobile index
    if($real_request == '/' && $is_mobile && !isset($_GET['s']))
    {
        if(mobile_pages_get_display_cookie() != 'desktop') {
            mobile_pages_set_cookie('mobile');
            wp_redirect(MOBILE_INDEX);
            exit();
        }
    }
	foreach($mobile_optional_patterns as $pattern) {
		preg_match("/$pattern/", $real_request_full, $matches);
        if(count($matches) > 0) {
			// If the cookie says its either mobile or desktop, it is
            if(mobile_pages_get_display_cookie() == 'mobile') {
            	return wp_get_theme($mobile_theme);
            } else {
                if(mobile_pages_get_display_cookie() == 'desktop')
            		return wp_get_theme($desktop_theme);
            }
            
            if($is_mobile) {
                mobile_pages_set_cookie('mobile');
                return wp_get_theme($mobile_theme);
            }
        }
	}
	mobile_pages_set_cookie('desktop');
	return wp_get_theme($desktop_theme);
}

function mobile_pages_set_cookie($display_type) {
  setcookie('mobile_pages_display_type', $display_type, time()+60*60*1*1, '/');
  $_COOKIE['mobile_pages_display_type'] = $display_type;
}

function mobile_pages_get_display_cookie() {
	if(array_key_exists('mobile_pages_display_type', $_COOKIE)) {
		if($_COOKIE['mobile_pages_display_type'] == 'mobile') {
			return 'mobile';
		} else {
            if($_COOKIE['mobile_pages_display_type'] == 'desktop')
    			return 'desktop';
		}
	}
	return '';
}

$wp_version = get_bloginfo( 'version' ); 
if($wp_version < 3.4){
	add_filter('the_content', 'mobile_wp_version_notice');
	function mobile_wp_version_notice($content) {
		$warning = '<script language="Javascript">';
		$warning .= 'alert ("Wordpress version is ' . get_bloginfo( "version" ) . '. You need at least version 3.4 in order for the RSMM mobile plugin to work")';
		$warning .= '</script>';
		return $warning . $content;
	}
}

add_action('init', 'create_default_pages');
function create_default_pages(){
	global $wpdb;
	
	$menuname = 'Mobile Menu';
	// if menu doesn't exist, create menu, pages and menu items
	if( !wp_get_nav_menu_object( $menuname )){
		$menu_id = wp_create_nav_menu($menuname);
	
		//http://wordpress.stackexchange.com/questions/58593/check-if-post-title-exists-insert-post-if-doesnt-add-incremental-to-meta-if
		$default_mobile_pages = array(
			array(
				'post_title' => 'About Us',
				'post_name' => 'index',
			),
			array(
				'post_title' => 'Services',
				'post_name' => 'services',
			),
			array(
				'post_title' => 'Video Gallery',
				'post_name' => 'videos',
				'post_content' => '[youtube_gallery]' . "\r" . '[youtube_thumb id="gzDS-Kfd5XQ" title="<strong>custom <em>title</em></strong>"]' . "\r" . '[youtube_thumb id="HE5RUNGfL_w"]'  . "\r" . '[/youtube_gallery]',
			),
			array(
				'post_title' => 'Office Tour',
				'post_name' => 'office-tour',
				'post_content' => '[mobile_gallery]',
			),
			array(
				'post_title' => 'Directions',
				'post_name' => 'directions',
			),
			array(
				'post_title' => 'Contact Us',
				'post_name' => 'contact-us',
				'post_content' => 'If you have a question or would like to know about our financial policies, just give our friendly office team a call.'. "\r" .'[mobile_contact_info ]',
			),
		);
		
		foreach($default_mobile_pages as $batch_page){
			$query = $wpdb->prepare(
				'SELECT ID FROM ' . $wpdb->posts . '
				WHERE post_title = %s
				AND post_type = \'mobile_page\'',
				$batch_page['post_title']
			);
			$wpdb->query( $query );
			if ( $wpdb->num_rows ) {
				$post_id = $wpdb->get_var( $query );
				$meta = get_post_meta( $post_id, 'times', TRUE );
				$meta++;
				update_post_meta( $post_id, 'times', $meta );
			} else {
				$new_post = array(
					'post_title' => $batch_page['post_title'],
					'post_name' => $batch_page['post_name'],
					'post_content' => $batch_page['post_content'],
					'post_status' => 'publish',
					'post_type' => 'mobile_page',
					'comment_status' => 'closed'
				);
			
				$post_id = wp_insert_post($new_post);
				add_post_meta($post_id, 'times', '1');
				
				/* Create menu item for mobile page */
			    wp_update_nav_menu_item($menu_id, 0, array(
			    	'menu-item-object-id' => $post_id,
			    	'menu-item-title' => $batch_page['post_title'],
			    	'menu-item-object' => 'mobile_page',
			    	'menu-item-type' => 'post_type',
			    	'menu-item-status' => 'publish'
			    	)
			    );
			}
		}
	}
}

if ( ! function_exists( 'nt_delete_htmltags' ) ){
	function nt_delete_htmltags($content,$paragraph_tag=false,$br_tag=false){	
		#$content = preg_replace('#<\/p>(\s)*<p>$|^<\/p>(\s)*<p>#', '', trim($content));
		$content = preg_replace('#^<\/p>|^<br \/>|<p>$#', '', $content);
		$content = preg_replace('#<br \/>#', '', $content);
		$content = str_replace('&nbsp;', '', $content);
		if ( $paragraph_tag ) $content = preg_replace('#<p>|</p>#', '', $content);
		return trim($content);
	}
}
if ( ! function_exists( 'nt_content_helper' ) ){
	function nt_content_helper($content,$paragraph_tag=true,$br_tag=false){
		return nt_delete_htmltags( do_shortcode(shortcode_unautop($content)), $paragraph_tag, $br_tag );
	}
}

if( !function_exists("curl_file_get_contents") ){
	function curl_file_get_contents($request){
		$curl_req = curl_init($request);
		curl_setopt($curl_req, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($curl_req, CURLOPT_HEADER, FALSE);
		$contents = curl_exec($curl_req);
		curl_close($curl_req);
		return $contents;
	}
}

add_shortcode("youtube_gallery", "youtube_gallery");
function youtube_gallery($atts, $content = null){
	if($content == ''){
		return;
	}
	//process nested shortcodes
	$content = nt_content_helper($content);
	
	$output = "<div class='video-gallery'>";
	$output.= $content;
	$output.= "</div>";
	$output.= "<div style='clear: both;'></div>" . "\n";
	return $output;
}
add_shortcode("youtube_thumb", "youtube_thumb_shortcode");
function youtube_thumb_shortcode($atts, $content = null){
	global $youtube_thumb_num;

	// if an intial thumb number hasn't been set, reset to 1
	if(!$youtube_thumb_num){$youtube_thumb_num = 1;}
	
	extract(shortcode_atts(array(
		"id" => 'false',
		"title" => "false"
	), $atts));
	if($id == 'false'){
		if ( is_user_logged_in() ) {
			$output = "<div class='gallery-item col-0'>" . "\n" .  "<div class='gallery-icon'>" . "\n";
			 $output.= "<span>Please specify a Youtube ID. Example: http://youtube.com/watch?v=<strong>gzDS-Kfd5XQ</strong> grab the bolded part. Then set the ID of the shortcode like so: [youtube_thumb id=\"<strong>gzDS-Kfd5XQ</strong>\"]</span>";
 			 $output.= "</div></div>" . "\n";
			 return $output;
		} else {
			return;
		}
	}

	
	$youtube_thumb_num++;
	
	$request_url = "http://gdata.youtube.com/feeds/api/videos/".$id."?v=2&alt=jsonc";

	/* check if video thumbnail exists */
	$ch = curl_init($request_url);
	curl_setopt($ch, CURLOPT_NOBODY, true);
	curl_exec($ch);
	$retcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	curl_close($ch);

	if($retcode != 200){
		//echo 'video not found';
		if ( is_user_logged_in() ) {
			$output = "<div class='gallery-item col-0'>" . "\n" .  "<div class='gallery-icon'>" . "\n";
			 $output.= "<span>The Youtube ID specified is not a valid video. Please specify a valid youtube video ID</span>";
 			 $output.= "</div></div>" . "\n";
			 return $output;
		} else {
			return;
		}
	} else {
		/* Acquire json data using fopen (not supported by some servers) */
		//$dataResponse = file_get_contents($request_url);
		//$json = json_decode($dataResponse);

		/* Acquire json data using curl */
		$dataResponse = curl_file_get_contents($request_url);
		$json = json_decode($dataResponse);

		$total_thumb_num = substr_count(get_the_content(), "[youtube_thumb");
		
		if( $total_thumb_num > 1 ){
			$thumbnail_url = $json->data->thumbnail->sqDefault;
		} else {
			$thumbnail_url = $json->data->thumbnail->hqDefault;
		}

		if($title == "false"){
			$thumbnail_title = $json->data->title;
		} else {
			$thumbnail_title = $title;
		}

		$output = "<div class='gallery-item col-0'>" . "\n" .  "<div class='gallery-icon'>" . "\n";

		//include_once('Mobile_Detect.php');
		//$mobile_detect = new Mobile_Detect();
		//if($mobile_detect->isiOS()){
		    //$output.= '<iframe width="100%" height="100%" src="http://www.youtube.com/embed/' . $id . '?controls=0&showinfo=0&showsearch=0&modestbranding=0"></iframe>';
			//$output.= "<span class='video-title'>".$thumbnail_title."</span>";
		//} else {
			$output.= "<a href='http://youtube.com/watch?v=".$id."&autoplay=1'>" . "\n";
			//$output.= "<a href='http://www.youtube.com/embed/".$id."'>" . "\n";
			$output.= "<span class='video'><img width='320' height='240' title='".$thumbnail_title."' alt='".$thumbnail_title."' src='".$thumbnail_url."'></span>" . "\n";
			$output.= "<span class='video-title'>".$thumbnail_title."</span>" . "\n";
			$output.= "</a>";
		//}

		$output.= "</div></div>" . "\n";
		
		

		return $output;
	}
}