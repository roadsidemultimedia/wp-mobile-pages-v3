<?php
/**
 * Create a custom post type for Mobile Pages
 */

add_action( 'init', 'register_cpt_mobile_page', 10);

function register_cpt_mobile_page() {

	$labels = array( 
		'name' => _x( 'Mobile Pages', 'mobile_page' ),
		'singular_name' => _x( 'Mobile Page', 'mobile_page' ),
		'add_new' => _x( 'Add New', 'mobile_page' ),
		'add_new_item' => _x( 'Add New Mobile Page', 'mobile_page' ),
		'edit_item' => _x( 'Edit Mobile Page', 'mobile_page' ),
		'new_item' => _x( 'New Mobile Page', 'mobile_page' ),
		'view_item' => _x( 'View Mobile Page', 'mobile_page' ),
		'search_items' => _x( 'Search Mobile Pages', 'mobile_page' ),
		'not_found' => _x( 'No mobile pages found', 'mobile_page' ),
		'not_found_in_trash' => _x( 'No mobile pages found in Trash', 'mobile_page' ),
		'parent_item_colon' => _x( 'Parent Mobile Page:', 'mobile_page' ),
		'menu_name' => _x( 'Mobile Pages', 'mobile_page' ),
	);

	$args = array( 
		'labels' => $labels,
		'hierarchical' => false,
		'description' => 'Mobile specific pages for your mobile theme.',
		'supports' => array( 'title', 'editor', 'revisions' ),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 20,
		'menu_icon' => WP_PLUGIN_URL.'/'.str_replace(basename( __FILE__),"",plugin_basename(__FILE__)) . 'images/mobile-page-icon.png',
		'show_in_nav_menus' => true,
		'publicly_queryable' => true,
		'exclude_from_search' => true,
		'has_archive' => false,
		'query_var' => true,
		'can_export' => true,
		'rewrite' => array( 
			'slug' => 'mobile', 
			'with_front' => false,
			'feeds' => false,
			'pages' => false
		),
		'capability_type' => 'post'
	);
	
	register_post_type( 'mobile_page', $args );
	
	// Flush permalink structure
	$options = get_option('mobile_page_options');
	if( $options['links-flushed'] == 'false' ) {
		flush_rewrite_rules( 'false' ); // soft flush is enough
		$options['links-flushed'] = 'true';
		update_option( 'mobile_page_options', $options );
	}
}

?>