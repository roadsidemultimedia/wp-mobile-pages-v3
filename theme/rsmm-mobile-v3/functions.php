<?php
/**
 *
 * @package RSMM Mobile
 * @subpackage Functions
 * @version 3.0.7
 */

/* Load the core theme framework. */
require_once( trailingslashit( TEMPLATEPATH ) . 'library/hybrid.php' );
$hybrid = new Hybrid();
$hybrid->prefix = "rsmm_mobile";

add_action( 'wp_enqueue_scripts' , 'wp_mobile_enqueue_script' );
add_action( 'template_redirect' , 'post_list_filters' );
add_action( 'wp_head' , 'setHeaderMeta');

add_shortcode('mobile_wufoo_form' , 'mobile_wufoo_form');

/* Do theme setup on the 'after_setup_theme' hook. */
add_action( 'after_setup_theme', 'rsmm_mobile_theme_setup_theme' );

/**
 * Theme setup function.  This function adds support for theme features and defines the default theme
 * actions and filters.
 *
 * @since 0.9.0
 */
function rsmm_mobile_theme_setup_theme() {
	global $content_width;

	/* Get the theme prefix. */
	$prefix = hybrid_get_prefix();

	/* Add support for framework features. */
	add_theme_support( 'hybrid-core-menus', array( 'primary' ) );
	add_theme_support( 'hybrid-core-sidebars', array( 'primary', 'secondary', 'subsidiary', 'before-content', 'after-content', 'after-singular' ) );
	add_theme_support( 'hybrid-core-shortcodes' );
	add_theme_support( 'hybrid-core-template-hierarchy' );
	add_theme_support( 'hybrid-core-deprecated' );

	/* Add support for framework extensions. */
	add_theme_support( 'custom-field-series' );
	add_theme_support( 'get-the-image' );

	/* Add support for WordPress features. */
	add_theme_support( 'automatic-feed-links' );
	
	/* Add main nav menu */
	add_action( "{$prefix}_after_header" , 'add_menu' );
	
	/* Add Wufoo Form */
	add_action( "{$prefix}_after_header" , 'add_wufoo_form' );
	add_action( "{$prefix}_header" , 'add_logo' );
	add_action( "{$prefix}_before_footer" , 'add_footer_content' );
	add_action( "wp_head", 'dynamic_styles');
	add_action( 'wp_footer', 'template_dir_var');
	add_action( 'wp_footer' , 'formBlur' );
	
	/* Add the title, byline, and entry meta before and after the entry. */
	add_action( "{$prefix}_before_entry", 'rsmm_mobile_entry_title' );
	add_action( "{$prefix}_before_entry", 'rsmm_mobile_byline' );
	add_action( "{$prefix}_after_entry", 'rsmm_mobile_entry_meta' );
	
	add_filter( 'rsmm_mobile_entry_title' , 'remove_links_from_headings' );
	
	/* Add the comment avatar and comment meta before individual comments. */
	add_action( "{$prefix}_before_comment", 'rsmm_mobile_avatar' );
	add_action( "{$prefix}_before_comment", 'rsmm_mobile_comment_meta' );

	/* Add Hybrid theme-specific body classes. */
	add_filter( 'body_class', 'rsmm_mobile_theme_body_class' );
	
	add_filter( 'pre_get_posts', 'wp_mobile_mobile_search_filter' );
	add_filter( 'sidebars_widgets' , 'disable_sidebar_widgets' );
		
	//Remove some of the default meta info
	add_filter( "hybrid_meta_author", '__return_false' );
	add_filter( "hybrid_meta_template", '__return_false' );
	add_filter( "hybrid_meta_copyright", '__return_false' );
}


/**
 * Destroy sidebars
 */
function disable_sidebar_widgets( $sidebars_widgets ) {
	$sidebars_widgets = array( false );
	return $sidebars_widgets;
}
/**
 * Remove Singular Page / Post Page title
 */
function remove_links_from_headings($title){
    if ( ( is_front_page() && !is_home() ) || is_page() ) {
        return '<h1 class="page-title entry-title">' . get_the_title() . '</h1>';
    } else if ( is_single() ) {
        return '<h1 class="post-title entry-title">' . get_the_title() . '</h1>';
    } else {
        return $title;
    }
}

/**
 *  Post List Filter
 *  Alter the markup for a mobile friendly layout
 */

function post_list_filters(){
		
	/* FILTERS ---------------------------------------- */
	if( is_page_template('page-blog.php') || is_home() || ( is_front_page() && is_home() ) || is_archive() || is_search() ){
		// remove entry byline. 
		add_filter( 'hybrid_entry_title' , 'wp_mobile_title' ); 
		
		// remove "posted in %category%" | %response number%"	
		add_filter( 'hybrid_entry_meta' , 'return_blank' );  
		
		// Show content as excerpts
		add_filter( 'the_content' , 'excerpts_for_post_lists');
		
		if(!is_search()){ //exclude search results
			// add a class ".excerpt-list" to the <body> tag
			add_filter( "body_class", 'add_excerpt_list_class' );
			
			// replace normal byline with calender icon to posts
			add_filter( "{$prefix}_byline" , 'wp_mobile_calendar_byline' ); 
		}
		
	}
	if(is_search()){
		
		//remove the excerpt
		add_filter('the_content', 'return_blank');
		
		// remove the entry byline
		add_filter( 'hybrid_byline' , 'return_blank' ); 
		
		//remove thumbnail images
		add_filter('get_the_image', 'return_blank');
		
	}
	
	
	
	function filter_excerpt_readmore($content) {
		global $post;
//		$more = '<a class="read_more" href="' . get_permalink() . '" title="' . the_title_attribute('echo=0') . '">View this entry</a>';
		$more_markup = '<a class="read_more" href="' . get_permalink() . '" title="' . the_title_attribute('echo=0') . '">Read more</a>';
		$content = str_replace('[...]', '', $content);
		return $content . $more_markup;
	}
	
	
	if(is_archive()){
		//remove thumbnail images
		add_filter( 'get_the_image', 'return_blank');
		add_filter( 'the_excerpt' , 'filter_excerpt_readmore') ;
	}
	
	/* ------------------------------------------------ */

	function return_blank($arg)
	{
	    return "";
	}

	
	//add class "excerpt-list" to the body
	function add_excerpt_list_class( $classes ) {
		$classes[] = 'excerpt-list';
		return array_unique($classes);
	}
	
	// Add read more button to excerpts
	// function custom_excerpt_more( $ellipsis ) {
	// 		global $post;
	// 		return '... <a href="' . get_permalink() . '" class="read_more">Read more</a>';
	// 	}
	// 	
	
	
	
	function wp_mobile_title($title) {
		//$title =  '<h2 class="entry-title">' . the_title() . '</h2>';
		?>
			<h2 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="post-title"><?php the_title(); ?></a></h2>
		<?
	}

	
	
	//USE EXCERPTS ON BLOG PAGE
	function excerpts_for_post_lists($content = false) {
		$more_markup = '<a class="read_more" href="' . get_permalink() . '" title="' . the_title_attribute('echo=0') . '">Read more</a>';

		// If is the blog page, an archive, or search results
		if(is_home() || is_archive() || is_search()) :
			global $post;
			$content = $post->post_excerpt;

			// If an excerpt is set in the Optional Excerpt box
			if($content) :
				$content = apply_filters('the_excerpt', $content);

			// If no excerpt is set
			else :	
				$content = $post->post_content;
				//strip tags from excerpt
				$content = strip_shortcodes($content);
				$content = str_replace(']]>', ']]&gt;', $content);
				$content = strip_tags($content);
				$excerpt_length = 80;
				$words = explode(' ', $content, $excerpt_length + 1);

				//if the content is longer than the limit
				if(count($words) > $excerpt_length) :
					array_pop($words);
					//array_push($words, '...', $more_markup);
					array_push($words, '...', '');
				endif;
					$content = implode(' ', $words);
					$content = '<p>' . $content . '</p>';
					//endif;
			endif;
		endif;

		// Make sure to return the content
		//$content.= 
		return $content . $more_markup;
	}
}

/**
 *  Search Result Filter
 *  Limit search results to posts and mobile pages. Doesn't display any standard pages
 */
function wp_mobile_mobile_search_filter($query) {
    if ( !$query->is_admin && $query->is_search) {
		$query->set( 'post_type', array( 'mobile_page', 'post') );    
    }
	return $query;
}


/**
 *
 *
 *
 *           ACTIONS
 *
 *
 *
 */

// Point favicon to root of site
add_action('wp_head', 'blog_favicon');
function blog_favicon() {
	echo '<link rel="Shortcut Icon" type="image/x-icon" href="'.site_url().'/favicon.ico" />' . "\n";
}

add_action('init', 'register_wp_mobile_photoswipe');
add_action('wp_footer', 'print_wp_mobile_photoswipe');
function register_wp_mobile_photoswipe() { 
	wp_register_script( 'klass', 'http://cdn.jsdelivr.net/klass/1.2.2/klass.min.js', array(), '1.2.2', true);
	wp_register_script( 'photoswipe', 'http://cdn.jsdelivr.net/photoswipe/3.0.5/code.photoswipe.jquery-3.0.5.min.js', array( 'jquery', 'klass'), true);
}
function print_wp_mobile_photoswipe() {
	global $wp_mobile_photoswipe_scripts_printed;
	if ( ! $wp_mobile_photoswipe_scripts_printed )
		return;
	
	wp_print_scripts('klass');
	wp_print_scripts('photoswipe');
}

/**
 *  Queue up javascript
 */
function wp_mobile_enqueue_script() {
	wp_deregister_script( 'jquery' );
	wp_register_script( 'jquery', 'http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js', false);
	wp_enqueue_script("jquery");
		
	wp_enqueue_script( 'js_addons', get_stylesheet_directory_uri() . '/js/js_addons.js', array( 'jquery' ), '0.1' );	
	
	//wp_enqueue_script( 'klass', 'http://cdn.jsdelivr.net/klass/1.2.2/klass.min.js', array(), '1.2.2', true);	
	wp_enqueue_script( 'jquery_form', 'http://cdn.jsdelivr.net/jquery.form/3.24/jquery.form.js', array( 'jquery' ), '1.0', true);
	wp_enqueue_script( 'isotope', 'http://cdn.jsdelivr.net/isotope/1.5.24/jquery.isotope.min.js', array( 'jquery' ), '1.5.24', true);
}

/**
 * Remove Original Menu and add our custom menu
 */

function add_menu(){
	$mobile_options = get_option('mobile_page_options');
	?>
	<div id="menu-bar">
		
		<div id="button-rail">
			
			<?php if( $mobile_options['wufoo-form-id'] !== ''){ ?>
			<a href="#" id="wufoo-button" class="icon-envelope"></a>
			<?php } ?>
			
			<a class="phone icon-phone" href="tel:<? echo cinfotag('phone') ?>"><span><? echo cinfotag('phone') ?></span></a>
			
		</div>
		
		<div id="menu-button">
			<a class="icon-menu">MENU</a>
		</div>
		
	</div>
	<div id="primary-menu">
		<div class="menu">
		
		<?
		//Use Custom Menu named "Mobile Menu"
		if(function_exists('wp_nav_menu')) { ?>
			<div id="main-menu">
			<? wp_nav_menu(array(
				'menu' => 'Mobile Menu',
				'container' => '',
				'container_id' => 'primary-menu',
				'menu_id' => '',
				'fallback_cb' => 'hybrid_page_nav',
			));
			?>
			</div><!-- main-menu -->
			<?
		}
	?>
	</div> <!-- menu -->
	<div id="search"><? echo searchForm() ?></div>
	</div> <!-- primary-menu -->
	<?
}
function searchForm(){
	$defaultValue = 'Search this site...';
	//check if on search result page and show search query, otherwise display default value
	if ( is_search() ) $defaultValue = esc_attr( get_search_query() );
	// $form = '<form role="search" method="get" id="searchform" action="' . home_url( '/' ) . '" >
	$form = '<form method="get" id="searchform" action="' . home_url( '/' ) . '" >
    <div>
	    <input type="submit" id="searchsubmit" value="" />
    	<input type="text" value="' . $defaultValue . '" name="s" id="s" />
    </div>
    </form>';

    return $form;
}

function setHeaderMeta(){
	$mobile_options = get_option('mobile_page_options');
	// TODO: Add this in when the theme uses AJAX requests to navigate pages
	// until AJAX is used, this will just open a new safari window when a link is clicked
	//<meta name="apple-mobile-web-app-capable" content="yes" />
?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" />
	<meta name="msapplication-tap-highlight" content="no" />
	<meta name="format-detection" content="telephone=no">
	<?php if($mobile_options['touch-icon']): ?>
	<link rel="apple-touch-icon" href="<?php echo $mobile_options['touch-icon'] ?>" />
	<?php endif; ?>
	
<?php
}

/**
 *  Create template directory variable for Javascripts
 */
function template_dir_var() {
	echo '<script type="text/javascript">var templateDir = "'. get_bloginfo ('stylesheet_directory') .'";</script>'; 
}



add_shortcode('mobile_gallery', 'mobile_gallery');
function mobile_gallery($atts) {
	global $wp_mobile_photoswipe_scripts_printed;
	$wp_mobile_photoswipe_scripts_printed = true;
	
	extract(shortcode_atts(array(
		"captions" => "false",
	), $atts));
	
	?>
	<script type="text/javascript">
		jQuery(document).ready(function(){
			var myPhotoSwipe = jQuery(".gallery a").photoSwipe({ 
				enableMouseWheel: false,
				enableKeyboard: false,
				captionAndToolbarShowEmptyCaptions: false,
				allowUserZoom: true,
				captionAndToolbarAutoHideDelay: 0,
				captionAndToolbarFlipPosition: true
			});
		});
	</script>
	<?
	
	add_filter('body_class','mobile_gallery_caption');
	// add mobile css classes to the body
	if( $captions == "false" ){
	?>
		<script type='text/javascript'>
			jQuery(document).ready(function() {
				$('head').append('<style>.ps-caption{display:none !important;}</style>');
			});
		</script>
	<?php 
	}
	echo do_shortcode('[gallery link="file" itemtag="div" icontag="div" captiontag="span" columns="0"]');
}
add_filter( 'use_default_gallery_style', '__return_false' );

/**
 * Search Form: erase default value when search field receives focus. 
 * If nothing is entered, repopulate with default value when changing focus
 */
function formBlur(){
	?>
	<script type="text/javascript">
	jQuery(document).ready(function() {
		jQuery('input[type="text"]').focus(function() {
	        if (this.value == this.defaultValue){
	        	this.value = '';
	    	}
	        if(this.value != this.defaultValue){
		    	this.select();
	        }
	    });
	    jQuery('input[type="text"]').blur(function() {
	        if (this.value == ''){
	        	this.value = this.defaultValue;
	    	}
	    });
	});
	</script>
	<?php
}


/* Create wufoo form function */
function add_wufoo_form(){
	echo mobile_wufoo_form();
}
/* Function that returns wufoo form markup.
 * This allows it to be used as a shortcode
 */
function mobile_wufoo_form() {
	ob_start();
	?>
	<?php include 'includes/wufoo-form-code.php'; ?>
	<div id="contact-form">
		<h4>Send us a message</h4>
		<?php /* <form id="xmlForm" action="<? bloginfo ('stylesheet_directory') ?>/includes/wufoo-send.php" method="post"> */ ?>
		<form id="xmlForm" action="" method="post">
			<input id="Field1" name="Field1" class="field text medium" maxlength="255" type="text" value="Name:" />
			<input id="Field2" name="Field2" class="field text medium" maxlength="255" type="text" value="Phone:" />
			<input id="Field3" name="Field3" class="field text medium" maxlength="255" type="text" value="Email:" />
			<textarea id="Field4" name="Field4" class="field textarea medium" cols="50" rows="4" onFocus="clearText(this)">Message:</textarea>
			<input id="Field6"  name="Field6"  type="hidden" value="<?php echo the_title() ?>" />	
			<input id="Field10" name="Field10" type="hidden" value="<?php echo get_permalink() ?>" />
			<input type="hidden" name="wufoo_submit" />
			<input id="saveForm" class="btTxt" type="submit" value="Submit" />
		</form>
		<div id="formStatus"></div>
	</div>
	<?
	$ob_str=ob_get_contents();
	ob_end_clean();
	return $ob_str;
}

/*
 * Add Logo
 */
function add_logo(){
	$mobile_options = get_option('mobile_page_options');

	?>
    <div id='logobox'>
        <div id='logo'><img alt='Logo' class='retina' src='<?php echo $mobile_options['retina-logo-url']; ?>' /></div>
    </div>
<?}

/**
 * Dynamic Styles
*/
function dynamic_styles(){
	$mobile_options = get_option('mobile_page_options');
	?>
	<style>
		#header-container{background-color:<?php echo $mobile_options['header-bg-color']; ?>;}
		#logobox{height:<?php echo $mobile_options['header-height']; ?>px;}
		<?php echo $mobile_options['custom-css']; ?>
	</style>
<?}

/* Function that waits for submission
 * then it performs the submission and dies
 */
add_action('init', 'submit_wufoo');
function submit_wufoo(){
	if(isset($_POST['wufoo_submit'])) {
		
		$mobile_options = get_option('mobile_page_options');
		
		$subdomain = 'roadside';
		$apikey = '0SOH-X15E-9L2N-0FYE:X';
		$formid = $mobile_options['wufoo-form-id'];
		//$formid = 's7s0z9';
		
		$pairs = array();
		foreach($_POST as $key=>$val)
		{
			$pairs[] = urlencode($key) . "=" . urlencode($val);
		}
		$fieldsStr = implode("&", $pairs);
		
		$ref = curl_init('https://'.$subdomain.'.wufoo.com/api/v3/forms/'.$formid.'/entries.json'); 
		curl_setopt($ref, CURLOPT_HTTPHEADER, array('Content-type: application/x-www-form-urlencoded'));
		curl_setopt($ref, CURLOPT_POST, true);
		curl_setopt($ref, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ref, CURLOPT_POSTFIELDS,  $fieldsStr);
		curl_setopt($ref, CURLOPT_USERPWD, $apikey);
		curl_setopt($ref, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ref, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ref, CURLOPT_USERAGENT, 'Wufoo.com');
		echo curl_exec($ref);
		exit();
	}
}


/*
 * Add Footer content
 */
function add_footer_content(){
	?>
	<div id="footer-contact"><strong><? 
	if( cinfotag('company') ){
		echo cinfotag('company');
	} else {
		echo cinfotag('full-name');
	}
	?></strong>
	<br/>
		Phone: <a class="phone" href="tel:<? echo cinfotag('phone') ?>"><? echo cinfotag('phone') ?></a><br/>
		<? if(cinfotag('fax')) : ?>
		Fax: <? echo cinfotag('fax'); ?><br/>
		<? endif; ?>
		Email: <a class="email" href="mailto:<? echo cinfotag('email') ?>"><? echo cinfotag('email'); ?></a><br/>
		
		<br/>
		<? echo cinfotag('address1') ?><br/>
		<? echo cinfotag('address2') ?><br/>
	</div>
	
	<ul class='dark-nav social-menu'>
		
		<?php echo mobile_pages_item_enabled("youtube-url") && cinfotag('youtube-url') ? 
		'<li class="icon"><a class="external-link" href="' . cinfotag('youtube-url') . '"><img alt="Youtube" src="' . get_bloginfo ('stylesheet_directory') . '/images/icons/mobile-icon-youtube-2x.png"/>YouTube</a></li>' : ''; ?>
		
		<?php echo mobile_pages_item_enabled("facebook-url") && cinfotag('facebook-url') ? 
		'<li class="icon"><a class="external-link" href="' . cinfotag('facebook-url') . '"><img alt="Facebook" src="' . get_bloginfo ('stylesheet_directory') . '/images/icons/mobile-icon-facebook-2x.png"/>Facebook</a></li>' : ''; ?>
		
		<?php echo mobile_pages_item_enabled("twitter-url") && cinfotag('twitter-url') ? 
		'<li class="icon"><a class="external-link" href="' . cinfotag('twitter-url') . '"><img alt="Twitter" src="' . get_bloginfo ('stylesheet_directory') . '/images/icons/mobile-icon-twitter-2x.png"/>Twitter</a></li>' : ''; ?>
		
		<?php echo mobile_pages_item_enabled("review-google-url") && cinfotag('review-google-url') ? 
		'<li class="icon"><a class="external-link" href="' . cinfotag('review-google-url') . '"><img alt="Google Places" src="' . get_bloginfo ('stylesheet_directory') . '/images/icons/mobile-icon-google-places-2x.png"/>Write a Review</a></li>' : ''; ?>
		
		<?php echo mobile_pages_item_enabled("google-plus-url") && cinfotag('google-plus-url') ? 
		'<li class="icon"><a class="external-link" href="' . cinfotag('google-plus-url') . '"><img alt="Google+" src="' . get_bloginfo ('stylesheet_directory') . '/images/icons/mobile-icon-google-plus-2x.png"/>Google+</a></li>' : ''; ?>
		
		<?php echo mobile_pages_item_enabled("linkedin-url") && cinfotag('linkedin-url') ? 
		'<li class="icon"><a class="external-link" href="' . cinfotag('linkedin-url') . '"><img alt="LinkedIn" src="' . get_bloginfo ('stylesheet_directory') . '/images/icons/mobile-icon-linkedin-2x.png"/>LinkedIn</a></li>' : ''; ?>
		
		<?php echo mobile_pages_item_enabled("flickr-url") && cinfotag('flickr-url') ? 
		'<li class="icon"><a class="external-link" href="' . cinfotag('flickr-url') . '"><img alt="Flickr" src="' . get_bloginfo ('stylesheet_directory') . '/images/icons/mobile-icon-flickr-2x.png"/>Flickr</a></li>' : ''; ?>
				
		
		
	</ul>
	<div id="copyright">© <? echo mobile_dynamic_copyright_date() ?>, <? echo cinfotag('full-name') ?>, all rights reserved</div>
	<ul class='dark-nav' style='border-bottom: none;'>
		<li><a class="external-link" href='<? echo site_url(); ?>/?mobile=false'>View full desktop version</a></li>
	</ul>
	<?
}

/**
 * Function for adding Hybrid theme <body> classes.
 *
 * @since 0.9.0
 */
function rsmm_mobile_theme_body_class( $classes ) {
	global $wp_query, $is_lynx, $is_gecko, $is_IE, $is_opera, $is_NS4, $is_safari, $is_chrome;

	/* Singular post classes (deprecated). */
	if ( is_singular() ) {

		if ( is_page() )
			$classes[] = "page-{$wp_query->post->ID}"; // Use singular-page-ID

		elseif ( is_singular( 'post' ) )
			$classes[] = "single-{$wp_query->post->ID}"; // Use singular-post-ID
	}

	/* Browser detection. */
	$browsers = array( 'gecko' => $is_gecko, 'opera' => $is_opera, 'lynx' => $is_lynx, 'ns4' => $is_NS4, 'safari' => $is_safari, 'chrome' => $is_chrome, 'msie' => $is_IE );
	foreach ( $browsers as $key => $value ) {
		if ( $value ) {
			$classes[] = $key;
			break;
		}
	}

	/* Hybrid theme widgets detection. */
	foreach ( array( 'primary', 'secondary', 'subsidiary' ) as $sidebar )
		$classes[] = ( is_active_sidebar( $sidebar ) ) ? "{$sidebar}-active" : "{$sidebar}-inactive";

	if ( in_array( 'primary-inactive', $classes ) && in_array( 'secondary-inactive', $classes ) && in_array( 'subsidiary-inactive', $classes ) )
		$classes[] = 'no-widgets';

	/* Return the array of classes. */
	return $classes;
}

/**
 * Displays the post title.
 *
 * @since 0.5.0
 */
function rsmm_mobile_entry_title() {
	echo apply_atomic_shortcode( 'entry_title', '[entry-title]' );
}

/**
 * Default entry byline for posts.
 *
 * @since 0.5.0
 */
function rsmm_mobile_byline() {

	
	$byline = '';
	if ( 'post' == get_post_type() && 'link_category' !== get_query_var( 'taxonomy' ) && !is_single() && !is_search() ){
		$byline .= '<div class="calendar">';
		$byline .= '<div class="calendar-month">';
		$byline .= get_the_time('M');
		$byline .= '</div>';
		$byline .= '<div class="calendar-day">';
		$byline .= get_the_time('j');
		$byline .= '</div>';
		$byline .= '</div>';
	}
	// if ( 'post' == get_post_type() && 'link_category' !== get_query_var( 'taxonomy' ) )
	// $byline = '<p class="byline">' . __( 'By [entry-author] on [entry-published] [entry-edit-link before=" | "]', hybrid_get_textdomain() ) . '</p>';
	
	echo apply_atomic_shortcode( 'byline', $byline );
}
function wp_mobile_calendar_byline() {
	?>
	
	<?
}

/**
 * Displays the default entry metadata.
 *
 * @since 0.5.0
 */
function rsmm_mobile_entry_meta() {

	$meta = '';

	if ( 'post' == get_post_type() )
		$meta = '';
		//$meta = '<p class="entry-meta">' . __( '[entry-terms taxonomy="category" before="Posted in "] [entry-terms taxonomy="post_tag" before="| Tagged "] [entry-comments-link before="| "]', hybrid_get_textdomain() ) . '</p>';

	// elseif ( is_page() && current_user_can( 'edit_page', get_the_ID() ) )
	// 	$meta = '<p class="entry-meta">[entry-edit-link]</p>';

	echo apply_atomic_shortcode( 'entry_meta', $meta );
}

/**
 * Function for displaying a comment's metadata.
 *
 * @since 0.7.0
 */
function rsmm_mobile_comment_meta() {
	echo apply_atomic_shortcode( 'comment_meta', '<div class="comment-meta comment-meta-data">[comment-author] [comment-published] [comment-permalink before="| "] [comment-edit-link before="| "] [comment-reply-link before="| "]</div>' );
}

/**
 * Loads the loop-nav.php template with backwards compability for navigation-links.php.
 *
 * @since 0.2.0
 * @uses locate_template() Checks for template in child and parent theme.
 */
function rsmm_mobile_navigation_links() {
	locate_template( array( 'navigation-links.php', 'loop-nav.php' ), true );
}

/**
 * @since 0.7.0
 * @deprecated 0.9.0
 */
function rsmm_mobile_disable_styles() {
	_deprecated_function( __FUNCTION__, '0.9.0' );
}

/**
 * @since 0.4.0
 * @deprecated 0.9.0
 */
function rsmm_mobile_favicon() {
	_deprecated_function( __FUNCTION__, '0.9.0' );
}

/**
 * @since 0.4.0
 * @deprecated 0.9.0
 */
function rsmm_mobile_feed_link( $output, $feed ) {
	_deprecated_function( __FUNCTION__, '0.9.0' );
	return $output;
}

/**
 * @since 0.4.0
 * @deprecated 0.9.0
 */
function rsmm_mobile_other_feed_link( $link ) {
	_deprecated_function( __FUNCTION__, '0.9.0' );
	return $link;
}

function mobile_dynamic_copyright_date() {
	global $wpdb;
	$copyright_dates = $wpdb->get_results("
		SELECT
		YEAR(min(post_date_gmt)) AS firstdate,
		YEAR(max(post_date_gmt)) AS lastdate
		FROM
		$wpdb->posts
		WHERE
		post_status = 'publish'
	");
	$output = '';
	if($copyright_dates) {
		$copyright = $copyright_dates[0]->firstdate;
		if($copyright_dates[0]->firstdate != $copyright_dates[0]->lastdate) {
			$copyright .= '-' . $copyright_dates[0]->lastdate;
		}
		$output = $copyright;
	}
	return $output;
}

/* Contact info shortcode */
add_shortcode("mobile_contact_info", "mobile_contact_info_shortcode");
function mobile_contact_info_shortcode($atts, $content = null) {
	$content = "";
	extract(shortcode_atts(array(
		"address1" => 'address1',
		"address2" => 'address2',
		"phone" => 'phone',
		"email" => 'email',
		"map_url" => 'map-url',
	), $atts));
	ob_start();
	
	// Phone 
	if(cinfotag($phone) && $phone !== 'false'):
	?>
	<div class="rounded_btn">
		<span>phone</span>
		<a href="tel:<? echo cinfotag($phone) ?>"><? echo cinfotag($phone) ?></a>
	</div>
	
	<? 
	endif;
	
	// Email
	if( cinfotag($email) && $email !== 'false' ):
	?>
	<div class="rounded_btn">
		<span>email</span>
		<a href="mailto:<? echo cinfotag($email) ?>">
			<? echo cinfotag($email) ?>
		</a>
	</div>
	<?
	endif;
	
	// Map
	if( $map_url !== 'false'):
	?>
	<div class="rounded_btn">
		<span>map</span>
		<a class="multiline" href="http://maps.apple.com/maps?daddr=<? htmlentities( get_cinfotag($address1) ) ?>+<? htmlentities( get_cinfotag($address2) ) ?>">
			<? echo cinfotag($address1) ?>, <? echo cinfotag($address2) ?>
		</a>
	</div>
	<? elseif($address1 !== 'false'): ?>
	<div class="rounded_box">
		<? echo cinfotag($address1) ?>, <? echo cinfotag($address2) ?>
	</div>
	<?
	endif;
	
	$content=ob_get_contents();
	ob_end_clean();

	return $content;
}

/* 
 * 
 * remove unused plugins that don't apply to mobile pages 
 * 
 */

add_filter('addthis_post_exclude', 'my_addthis_post_exclude_filter');
function my_addthis_post_exclude_filter($display){
	if ( 'mobile_page' == get_post_type() )
    	$display = "";
		return $display;
}

/* remove WP Socializer Plugin markup from mobile pages */
remove_action('wp_footer', 'wpsr_floatingbts_output');
remove_action('wp_head', 'wpsr_scripts_adder');
remove_action('wp_footer', 'wpsr_scripts_adder');
remove_action('wp_footer', 'wpsr_footer');
remove_filter('the_content', array($wpsr_content_op, 'output'));
remove_filter('the_excerpt', array($wpsr_excerpt_op, 'output'));

/* remove Facebook Fan box scripts from mobile pages */
remove_action( 'wp_footer', 'facebook_fanbox_with_css_footer', 99 );

/* remove subpage navigation scripts and CSS from mobile pages */
remove_action( 'init', 'init_subpages_navigation_plugin' );

/* Dequeue scripts and styles */
add_action('wp_enqueue_scripts', 'mobile_dequeue_unused_plugins');
function mobile_dequeue_unused_plugins(){
	
	/* Remove subpage navigation Plugin */
	wp_dequeue_script( 'subpages-navigation' );
	wp_dequeue_style( 'subpages-navigation' );
	
	/* Remove Colorbox */
	remove_action( 'wp_enqueue_scripts', 'powerpack_register_colorbox_styles' );
	remove_action( 'wp_enqueue_scripts', 'powerpack_register_colorbox_scripts' );
	remove_action( 'wp_head', 'colorbox_ie_css' );
	wp_dequeue_script( 'colorbox' );
	wp_dequeue_script( 'colorbox-classes' );
	
}

/* Remove advanced Spoiler Plugin */
if (class_exists('AdvSpoiler')) {
	$spoiler_plugin = AdvSpoiler::get_instance();
	remove_action('wp_head', array($spoiler_plugin, 'wp_head'), 15);
	remove_action('wp_head', array($spoiler_plugin, 'enqueue_script'), 5);
	remove_action('wp_head', array($spoiler_plugin, 'enqueue_style'), 5);
}

/* Remove Cleaner Gallery Plugin */
remove_action( 'plugins_loaded', 'cleaner_gallery_setup' );
remove_action( 'template_redirect', 'cleaner_gallery_enqueue_script' );
remove_action( 'template_redirect', 'cleaner_gallery_enqueue_style' );

/* Remove query strings from enqueued JS and CSS files for better caching support */
if( !function_exists('_remove_script_version') ){
	function _remove_script_version( $src ){
		$parts = explode( '?', $src );
		return $parts[0];
	}
	add_filter( 'script_loader_src', '_remove_script_version', 15, 1 );
	add_filter( 'style_loader_src', '_remove_script_version', 15, 1 );
}

?>