//hide toolbar
addEventListener('load', function() {
	setTimeout(scrollTo, 0, 0, 1);
}, false);

function hideAddressBar() {
	window.scrollTo(0, 1);
}

jQuery(document).ready(function($) {
	//remove Smart Seo links	
	$("a.sl").removeAttr('href');

	$('input[type="text"]').focus(function() {
        if (this.value == this.defaultValue){
        	this.value = '';
    	}
        if(this.value != this.defaultValue){
	    	this.select();
        }
    });
    $('input[type="text"]').blur(function() {
        if (this.value == ''){
        	this.value = this.defaultValue;
    	}
    });
	
	var menuStatus = 0;
	$('#menu-button').click(function () {
		$(document).click(function(){ 
			$("#menu-button").click(function(){ return false; });
			$("#search input#s").click(function(){ return false; });
			$(document).one("click", function() { 
				menuStatus = 0;
				$('#primary-menu').removeTransitionClass('show');
			}); 
		})
		if(menuStatus == 0){
			menuStatus = 1;
			$("#primary-menu").addTransitionClass('show');			
		} else {
			menuStatus = 0;
			$('#primary-menu').removeTransitionClass('show');
		}
	});
	
	var formStatus = 0;
	$('#wufoo-button').click(function () {
		if(formStatus == 0){
			formStatus = 1;
			$("#contact-form").addTransitionClass('show');
		} else {
			formStatus = 0;
			$('#contact-form').removeTransitionClass('show');
		}
    });

	// Isotope for video and gallery thumbnails
	var $container = jQuery('.video-gallery, .gallery');
	$container.imagesLoaded( function(){
		$container.isotope({ layoutMode : 'fitRows',itemSelector : '.gallery-item' });
	});
});


/* Jquery CSS Transitions */
(function(j){var c=j(document),h=j("<div/>").css({position:"absolute",top:-200,width:100,height:100,WebkitTransition:"top 0.001s linear",MozTransition:"top 0.001s linear",OTransition:"top 0.001s linear",transition:"top 0.001s linear"}),f="transition",b;function d(k){k.data.unbind(j.support.cssTransitionEnd,d).removeClass(f)}function e(k){this.addClass(f).width();this.bind(j.support.cssTransitionEnd,this,d).addClass(k);return this}function i(k){this.addClass(f).width();this.bind(j.support.cssTransitionEnd,this,d).removeClass(k);return this}function g(){clearTimeout(b);b=null;h.remove()}function a(k){g();j.support.cssTransition=true;j.support.cssTransitionEnd=k.type;j.fn.addTransitionClass=e;j.fn.removeTransitionClass=i;c.unbind("transitionend webkitTransitionEnd oTransitionEnd",a)}j.fn.addTransitionClass=j.fn.addClass;j.fn.removeTransitionClass=j.fn.removeClass;c.bind("transitionend webkitTransitionEnd oTransitionEnd",a).ready(function(){document.body.appendChild(h[0]);h.width();h.css({top:-300});b=setTimeout(function(){g();j.support.cssTransition=false;j.support.cssTransitionEnd=false},100)})})(jQuery);