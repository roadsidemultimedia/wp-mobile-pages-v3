<script type="text/javascript">
// handle hide/show for text field default values
jQuery(document).ready(function($){
	inputDefaults = {};
	$("#xmlForm input:text, #xmlForm textarea").clearDefaultText();
});
jQuery.fn.clearDefaultText = function() {
	return this.each(function(){
		var element = jQuery(this);
		inputDefaults[element.attr("id")] = element.val();
		element.focus(function() {
			if (element.val() == inputDefaults[element.attr("id")]) {
				element.val('');
			}
		});
		element.blur(function() {
			if (element.val() == '') {
				element.val(inputDefaults[element.attr("id")]);
			}
		});
	});
}
function validateWufoo() {
	var defaultFound = false;
	jQuery("#xmlForm input:text").each(function(){
		var element = jQuery(this);
		if (element.val() == inputDefaults[element.attr("id")]) {
			jQuery("#formStatus").html("<p><span class='wufoo-error'>A required field is missing. Please finish filling out the form</span></p>");
			defaultFound = true;
		}
	});
	if(defaultFound){
		return false;
	}
}

jQuery(document).ready(function($) { 
	$('#xmlForm').ajaxForm({ 
		dataType: 'json',
		beforeSubmit: validateWufoo,
		success: processWufooResponse
	});
});

function processWufooResponse(data) {
	if(data == null){
		jQuery("#formStatus").html("<p><span class='wufoo-error'>unknown error</span></p>");
	}
	if(data.Success == true){
		jQuery("#formStatus").html("<p><span class='wufoo-success'>Thank you for your submission!</span></p>");
		
		//make form slide up and disappear
		jQuery('#xmlForm').slideUp();
		
		//blank out the fields if succesfully submitted
		jQuery(':input','#xmlForm')
			.not(':button, :submit, :reset, :hidden')
			.val('')
			.removeAttr('checked')
			.removeAttr('selected');
	} else {
		jQuery("#formStatus").html("<p><span class='wufoo-error'>Submission failed</span></p>");
		jQuery.each(data.FieldErrors, function(i,item){
			jQuery("#formStatus").append("<p><span class='wufoo-error-description'>"+item.ErrorText+"</span></p>");
		});
	}
}
function clearText(thefield){
	if (thefield.defaultValue == thefield.value){
		thefield.value = "";
	}
}

</script>